import React, {Component} from 'react';

import ProductList from './List';
import ProductAdd from './Add';

class ProductView extends Component {
    render() {
      return (
          <div>
            <ProductList />
            <ProductAdd />
          </div>
      );
    }
  }
  
export default ProductView;