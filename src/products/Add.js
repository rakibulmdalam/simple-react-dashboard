import React, { Component } from 'react'

import Button from 'react-bootstrap/Button'; 
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';


class ProductAdd extends Component{
    constructor(props) {
        super(props);
        this.currencyAPIKey = '7f726bfde624f42223d179aca6cd422c';
        this.currencyAPIEnd = 'http://data.fixer.io/api/';
        this.currencyAPIAccessKey = 'http://data.fixer.io/api/latest?access_key=7f726bfde624f42223d179aca6cd422c';
        this.state = {
            title: '',
            currency: 'EUR',
            price: 0,
            category_id: ''
        };
        this.categories = []
        
    
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchCurrency = this.fetchCurrency.bind(this);
        
      }
    
    handleTitleChange(event) {
        this.setState({title: event.target.value});
    }
    handlePriceChange(event) {
        this.setState({price: event.target.value});
    }
    handleCurrencyChange(event) {
        this.setState({currency: event.target.value});
    }
    handleCategoryChange(event) {
        this.setState({category_id: event.target.value});
    }
    
    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state);
        console.log(this.refs.category_id.value);
        console.log(this.refs.currency.value);

        fetch('http://localhost:8080/api/v1/products/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state),
            method: 'POST'
        }).then(response => response.json())
        .then(data => {
            console.log(data);
        });
        
    }

    componentDidMount(event){
        //console.log('didmount');
        fetch('http://localhost:8080/api/v1/categories/')
            .then(response => response.json())
            .then(data => {
                console.log(data);                
                this.categories = data;
        });
    }
    fetchCurrency(){
        fetch('http://data.fixer.io/api/latest?access_key=7f726bfde624f42223d179aca6cd422c')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                alert("1 "+this.state.currency+" = "+ (1 / data.rates[this.state.currency]) + " EUR");
            });
    }

    render() {
        
        return (
            <div>
                <h3>Add new product</h3>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label column sm={2}>
                        Title
                        </Form.Label>
                        <Col sm={10}>
                        <Form.Control type="text" placeholder="Title" value={this.state.title} onChange={this.handleTitleChange}/>
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalPassword">
                        <Form.Label column sm={2}>
                            Price
                        </Form.Label>
                        <Col sm={10}>
                            <Form.Control type="number" placeholder="Product Price" value={this.state.price} onChange={this.handlePriceChange} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalPassword" onChange={this.fetchCurrency}>
                        <Form.Label column sm={2}>
                            Currency
                        </Form.Label>
                        <Col sm={10}>                            
                            <Form.Control as="select" ref="currency" value={this.state.currency} onChange={this.handleCurrencyChange} >
                                <option>EUR</option>
                                <option>USD</option>
                                <option>INR</option>
                                <option>CAD</option>
                                <option>AUD</option>
                            </Form.Control>
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalPassword">
                        <Form.Label column sm={2}>
                            Category
                        </Form.Label>
                        <Col sm={10}>
                            <Form.Control as="select" ref="category_id" value={this.state.category} onChange={this.handleCategoryChange} >
                            {
                                this.categories.map((item, index) => {
                                    return(
                                        <option key={index} value={item.id}>{item.name}</option>
                                    );
                                    
                                })
                            }
                            </Form.Control>
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Col sm={{ span: 10, offset: 2 }}>
                            <Button type="submit">Add Product</Button>
                        </Col>
                    </Form.Group>
                </Form>
            </div>
        );
    }
}

export default ProductAdd;