import React, { Component } from 'react';

import Table from 'react-bootstrap/Table'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Button from 'react-bootstrap/Button'


class ProductList extends Component {
    constructor(props) {
        super(props);

        this.state = {
          data: []
        };
    }

    componentDidMount() {
        fetch('http://localhost:8080/api/v1/products/')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ data: data });
            });
    }

    deleteProduct = prodId => {
        const requestOptions = {
            method: 'DELETE'
        };
        fetch("http://localhost:8080/api/v1/product/" + prodId, requestOptions).then((response) => {
            return response.json();
        }).then((result) => {
            console.log('response received');
            this.componentDidMount();
        });
    }
    render() {
        return(
            <div>
            <h3>List of Products <Button onClick={() => { this.componentDidMount() }} size="sm mx-2" variant="success" active>Reload</Button></h3>
            <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>
                        Title
                    </th>
                    <th>
                        Category
                    </th>
                    <th>
                        Price
                    </th>
                    <th>
                        currency
                    </th>
                    <th>
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
            {
                this.state.data.map((item, index) => {
                    return(
                    <tr key={index}>
                        <td>
                            {item.title}
                        </td>
                        <td>
                            {item.category.name}
                        </td>
                        <td>
                            {item.price}
                        </td>
                        <td>
                            {item.currency}
                        </td>
                        <td>
                        <ButtonToolbar>
                            {/* <Button size="sm mx-2" variant="success" active>
                            Edit
                            </Button> */}
                            <Button onClick={() => { this.deleteProduct(item.id) }} size="sm mx-2" variant="danger" active>Delete</Button>
                        </ButtonToolbar>
                        </td>
                    </tr>
                    )
                })
            }
            </tbody>
            </Table>
            
            </div>
        )
    }
}

export default ProductList;