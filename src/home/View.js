import React, {Component} from 'react';

import { BrowserRouter as Router, Link } from 'react-router-dom';


import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';


class HomeView extends Component{
    render(){
        return(        
            <div>
                <Container>
                    <Row>
                        <Col>
                            <Card bg="success" align="center" text="white" style={{ width: '26rem' }} >
                                <Card.Body>
                                    <Card.Title>Product Management</Card.Title>
                                    <Card.Text>
                                    <Link to={'/products'}>view/add/delete products.</Link>
                                    </Card.Text>
                                    
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card bg="info" align="center" text="white" style={{ width: '26rem' }}>
                                <Card.Body>
                                    <Card.Title>Category Management</Card.Title>
                                    <Card.Text>
                                    <Link to={'/categories'}>view/add/delete categories.</Link>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
        
    }
}

export default HomeView;