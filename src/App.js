import React, {Component} from 'react';

import { BrowserRouter as Router, Route, Redirect, Link } from 'react-router-dom';
import HomeView from './home/View';
import ProductView from './products/View';
import CategoryView from './categories/View';


import './App.css';



class App extends Component{
    render(){
        return(
            <div className='page'>            
                <Router>
                    <div>
                    <h2 align="center">Welcome to a simple dashboard</h2>
                    <nav align="center" className="navbar navbar-expand-lg navbar-light bg-light">
                    <ul className="navbar-nav mr-auto">
                        <li><Link to={'/home'} className="nav-link"> Home </Link></li>
                        <li><Link to={'/products'} className="nav-link">Products</Link></li>
                        <li><Link to={'/categories'} className="nav-link">Categories</Link></li>
                    </ul>
                    </nav>
                    <hr />
                    <Redirect from="/" to="/home" />
                    <Route path='/home' component={HomeView} />
                    <Route path='/products' component={ProductView} />
                    <Route path='/categories' component={CategoryView} />
                    
                    </div>
                </Router>
            </div>
        )
        
    }
}

export default App;