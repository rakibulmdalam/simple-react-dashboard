import React, { Component } from 'react';
import Table from 'react-bootstrap/Table'

import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Button from 'react-bootstrap/Button'

class CategoryList extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          data: []
        };
    }
    
    componentDidMount() {
        fetch('http://localhost:8080/api/v1/categories/')
            .then(response => response.json())
            .then(data => {
                this.setState({ data });
            });
            
    }

    deleteCategory = catId => {
        const requestOptions = {
            method: 'DELETE'
        };
        fetch("http://localhost:8080/api/v1/category/" + catId, requestOptions).then((response) => {
            return response.json();
        }).then((result) => {
            console.log('response received');
            this.componentDidMount();
        });
    }
      
    

    render() {
        return(
            <div>
                <h3>List of Categories</h3>
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Path
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.data.map((item, index) => {
                        //this.state.data.map(function(item, index, this) {
                            //console.log(index);
                            return(
                            <tr key={index}>
                                <td>
                                    {item.name}
                                </td>
                                <td>
                                    {item.path}
                                </td>
                                <td>
                                <ButtonToolbar>                                   
                                    <Button onClick={() => { this.deleteCategory(item.id) }} size="sm mx-2" variant="danger" active>Delete</Button>
                                </ButtonToolbar>
                                </td>
                            </tr>
                            )
                        })
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default CategoryList;